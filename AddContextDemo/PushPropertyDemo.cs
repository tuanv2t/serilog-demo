﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;

namespace AddContextDemo
{
    public class PushPropertyDemo
    {
        public static async Task WriteLogAAsync()
        {
            Log.Logger.Information("Something happened in WriteLogAAsync");
        }
        public static async Task WriteLogBAsync()
        {
            Log.Logger.Information("Something happened in WriteLogBAsync");
        }

        static async Task MainAsync()
        {
            await WriteLogAAsync();
            await WriteLogBAsync();
        }
        public static void RunDemo()
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .Enrich.FromLogContext()// NOTE: To use PushProperty , it's necessary to include   .Enrich.FromLogContext()
               .WriteTo.LiterateConsole()
               .WriteTo.RollingFile("logs\\myapp-{Date}.txt",
                   outputTemplate: "{Timestamp:u} [{Level}] ({Application}){PersonDetails} {Message}{NewLine}{Exception}")
               //.WriteTo.File(@"C:\Log.txt",
               //outputTemplate: "{Timestamp:u} [{Level}] ({Application}) {Message}{NewLine}{Exception}")
               .CreateLogger();

            using (LogContext.PushProperty("Application", "Demo"))
            {
                MainAsync().Wait();
            }
            //The content of the output file look like this
            //2016 - 10 - 26 11:21:33Z[Information]() Something happened in WriteLogAAsync
            //2016 - 10 - 26 11:21:33Z[Information]() Something happened in WriteLogBAsync

            //It does not contain the Application as expected :(
            var person = new Person()
            {
                FirstName = "Tuan",
                LastName = "Vo"
            };
            var coveredObj = Serilog.Context.LogContext.PushProperty("PersonDetails", person, true);
            Log.Logger.Information("This log entry includes a structured object Person");
            //you can do something here
            Log.Logger.Information("This log entry includes the structured object Person as well");

            //remove the person from the context ( stact )

            coveredObj.Dispose();
            Log.Logger.Information("This log entry does not include the structured object Person");
            /*
             *The output file looks like
             *  2016-12-13 08:00:03Z [Information] (Demo) Something happened in WriteLogAAsync
                2016-12-13 08:00:03Z [Information] (Demo) Something happened in WriteLogBAsync
                2016-12-13 08:00:03Z [Information] ()Person { FirstName: "Tuan", LastName: "Vo" } This log entry includes a structured object Person
                2016-12-13 08:00:03Z [Information] ()Person { FirstName: "Tuan", LastName: "Vo" } This log entry includes the structured object Person as well
                2016-12-13 08:00:03Z [Information] () This log entry does not include the structured object Person

             */
        }
    }
}
