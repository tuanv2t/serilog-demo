﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace Enrich.WithPropertyDemo
{
    class Program
    {
        private static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.RollingFile("logs\\myapp-{Date}.txt")
                .WriteTo.LiterateConsole()
                .Enrich.WithProperty("Application", "e-Commerce")
                .Enrich.WithProperty("Environment", ConfigurationManager.AppSettings["Environment"])
                .CreateLogger();

            Log.Information("Application version {Version} starting up",
                typeof(Program).Assembly.GetName().Version);
        }
    }
}
