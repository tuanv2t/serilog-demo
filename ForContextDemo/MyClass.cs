﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace ForContextDemo
{
    public class MyClass
    {
        private Serilog.ILogger m_log;
        public MyClass()
        {
            //m_log = Log.Logger.ForContext<MyClass>();
            //m_log = Log.ForContext();
        }

        public void WriteHelloLog()
        {
            // This will have a property called "SourceContext" which is equal to "MyNamespace.MyClass"
            //m_log.Information("Hello!");
        }
    }
}
