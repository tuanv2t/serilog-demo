﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
namespace ForContextDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
          .MinimumLevel.Debug()
          .WriteTo.LiterateConsole()
          //.WriteTo.RollingFile("logs\\myapp-{Date}.txt")
          .WriteTo.File(@"C:\Log.txt",
           outputTemplate: "{Timestamp:u} [{Level}] ({Application}) {Message}{NewLine}{Exception}")
          .CreateLogger();


            Log.ForContext("Application", "Demo")
            .Information("Adding {Item} to cart {CartId}", "Item", "123");
            //The output file Log.text contains the line like this: 2016-10-26 09:00:43Z [Information] (Demo) Adding "Item" to cart "123"
            //Note that the {Application} has been replaced with "Demo"
        }
    }
}
